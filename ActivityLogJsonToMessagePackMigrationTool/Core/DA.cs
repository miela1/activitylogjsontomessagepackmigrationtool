﻿using System;
using System.Data;
using ActivityLogJsonToMessagePackMigrationTool.Config;
using System.Data.SqlClient;

namespace ActivityLogJsonToMessagePackMigrationTool.Core
{
	public class DA : IDisposable
	{
		ConnectionsNames ConnectionName;

		SqlConnection Connection;
		SqlTransaction Transaction;

		bool TransactionResult = true;

		public DA(ConnectionsNames name)
		{
			ConnectionName = name;
		}
		public DA OpenConnection()
		{
			if (Connection == null)
			{
				//Log.Console.Debug("ConnectionName", GetConnectionString(ConnectionName));
				Connection = new SqlConnection(GetConnectionString(ConnectionName));
			}

			if (Connection.State != ConnectionState.Open)
				Connection.Open();

			return this;
		}
		public void CloseConnection()
		{
			Connection.Close();
		}

		public SqlDataReader ExecuteQuery(string sql, params SqlParameter[] parameters)
		{
			if (Connection == null)
				_ = OpenConnection();

			var command = new SqlCommand(sql, Connection);
			command.Parameters.AddRange(parameters);

			return command.ExecuteReader();

		}
		public int Execute(string sql, params SqlParameter[] parameters)
		{
			if (Connection == null)
				_ = OpenConnection();

			try
			{
				SqlCommand command;
				if (Transaction != null)
					command = new SqlCommand(sql, Connection, Transaction);
				else
					command = new SqlCommand(sql, Connection);

				command.Parameters.AddRange(parameters);
				return command.ExecuteNonQuery();

			}
			catch (Exception ex)
			{
				//Log.Both.ErrorEx("Failed to execute sql", ex);
				if (Transaction != null)
					TransactionResult = false;
			}

			return 0;
		}
		public void UseTransactions(string name)
		{
			if (Transaction == null)
			{
				TransactionResult = true;
				Transaction = Connection.BeginTransaction(name);
				//Log.Both.Debug("new tansaction", name);
			}
		}

		public void RolebackTransactions()
		{
			if (Transaction == null)
				Transaction.Rollback();
		}
		public bool CommitTransaction()
		{
			if (!TransactionResult)
			{
				//Log.Both.Error("Transaction failed do to en error in one or more sql statements");
				//Log.Both.Error("Rolling back transaction");
				Transaction.Rollback();
			}
			else
			{
				//Log.Both.Debug("Commiting transaction");
				Transaction.Commit();
			}


			Transaction.Dispose();
			Transaction = null;

			return TransactionResult;
		}

		public string GetConnectionString(ConnectionsNames name)
		{
			switch (name)
			{
				default:
				case ConnectionsNames.WebtrackStatsCollection:
					return Settings.Databases.ConnectionStrings.WebTrackStatsCollection;
			}
		}

		public SqlParameter MakeParameter(string name, object value, SqlDbType dbType)
		{
			return new SqlParameter
			{
				ParameterName = name,
				SqlDbType = dbType,
				Value = value
			};
		}
		public SqlParameter MakeParameter(string name, object value, SqlDbType dbType, int size)
		{
			return new SqlParameter
			{
				ParameterName = name,
				SqlDbType = dbType,
				Value = value,
				Size = size
			};
		}

		public void Dispose()
		{
			//Log.Console.Debug("Disposing DA");
			if (Transaction != null)
				Transaction.Dispose();
			Connection.Close();
			Connection.Dispose();
		}
	}

	public enum ConnectionsNames
	{
		WebtrackStatsCollection = 0
	}

	public static class DataAccess
	{
		public static DA WebtrackStatsCollection => new DA(ConnectionsNames.WebtrackStatsCollection);
	}
}
