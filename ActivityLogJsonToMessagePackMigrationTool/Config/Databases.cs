﻿using Newtonsoft.Json.Linq;

namespace ActivityLogJsonToMessagePackMigrationTool.Config
{
	public class Databases
	{
		public Databases(JToken json)
		{
			ConnectionState = json["ConnectionState"].ToString();
			ConnectionStrings = new ConnectionStrings(json["ConnectionStrings"], ConnectionState);
		}
		public string ConnectionState { get; }
		public ConnectionStrings ConnectionStrings { get; }
	}
	public class ConnectionStrings
	{
		public ConnectionStrings(JToken json, string state)
		{
			WebTrackStatsCollection = json[$"WebTrackStatsCollection_{state}"].ToString();
		}
		public string WebTrackStatsCollection { get; }

	}
}
