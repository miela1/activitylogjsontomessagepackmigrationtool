﻿using r2pCore.Common.Logging.Basic;
using System;

namespace ActivityLogJsonToMessagePackMigrationTool.Core.Dataitems
{
	public class ActivityLogDbData:IDisposable
	{
		public int CustomerId { get; set; }
		public int UserId { get; set; }

		public string Url { get; set; }
		public string Action { get; set; }
		public string JsonData { get; set; }

		public byte[] MessagePackData { get; set; }

		public DateTime Timestamp { get; set; }

		public void Dispose()
		{
			Log.Console.Information("Disposing ActivityLogDbData");
		}
	}
}
