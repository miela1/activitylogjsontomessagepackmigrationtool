﻿using MessagePack;

namespace ActivityLogJsonToMessagePackMigrationTool.Core.Dataitems
{
	[MessagePackObject]
	public class ActivityLogPackData
	{
		[Key(0)]
		public string Action { get; set; }
		[Key(1)]
		public string Url { get; set; }
		[Key(2)]
		public string Timestamp { get; set; }
		[Key(3)]
		public string TargetElement { get; set; }
		[Key(4)]
		public string TargetSelect { get; set; }
		[Key(5)]
		public BrowserInfo BrowserInfo { get; set; }
		[Key(6)]
		public JSSize ScreenSize { get; set; }
		[Key(7)]
		public JSSize WindowSize { get; set; }
		[Key(8)]
		public JSPosition MousePosition { get; set; }

	}

	[MessagePackObject]
	public class BrowserInfo
	{
		[Key(0)]
		public string Language { get; set; }
		[Key(1)]
		public string UserAgent { get; set; }
		[Key(2)]
		public bool DarkMode { get; set; }
	}
	[MessagePackObject]
	public class JSSize
	{
		[Key(0)]
		public int Width { get; set; }
		[Key(1)]
		public int Height { get; set; }
	}

	[MessagePackObject]
	public class JSPosition
	{
		[Key(0)]
		public int X { get; set; } = 0;
		[Key(1)]
		public int Y { get; set; } = 0;
	}

	public class ActivityLogJsonData : ActivityLogPackData 	{ }
}
