﻿using System;
using System.Threading;
using ActivityLogJsonToMessagePackMigrationTool.Config;
using ActivityLogJsonToMessagePackMigrationTool.Core;
using ActivityLogJsonToMessagePackMigrationTool.Core.Dataitems;
using MessagePack;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ActivityLogJsonToMessagePackMigrationTool
{
	class Program
	{
		static bool keepRunning = true;

		static void Main(string[] args) {
			//Log.Console.Information("Starting migration tool");

			Thread.Sleep(1000);
			while (keepRunning) {
				ParseNext();
			}

			//HandleExit();
		}

		static Datafactory factory;
		private static void ParseNext() {
			//Log.Console.Information("Parser run");
			if (factory == null)
				factory = new Datafactory();

			var data = factory.GetActivityLogItems(Settings.AppSettings.BatchSize);
			foreach (var item in data) {
				try {
					//Log.Console.Information($"Converting item {item.CustomerId,8} {item.UserId,8} {item.Timestamp.ToFileTime()} {item.Action,20} {item.Url,-60}");
					var tmp = JsonConvert.DeserializeObject<ActivityLogJsonData>(item.JsonData);
					item.MessagePackData = MessagePackSerializer.Serialize<ActivityLogPackData>(tmp);
				} catch {
					var t2 = JObject.Parse(item.JsonData);
					t2["MousePosition"]["x"] = 0;
					t2["MousePosition"]["y"] = 0;
					var t3 = JsonConvert.DeserializeObject<ActivityLogJsonData>(t2.ToString());
					item.MessagePackData = MessagePackSerializer.Serialize<ActivityLogPackData>(t3);

					//Log.Console.Error($"Failed to convert item {item.CustomerId,8} {item.UserId,8} {item.Timestamp.ToFileTime()} {item.Action,20} {item.Url,-60}");
				}
			}
			var result = factory.SaveActivityLogItems(data);
			if (result > 0) {
				//Log.Console.Information($"Done");
				//Log.Console.Information("");
				//ParseNext();
			} else {
				//Log.Console.Information("No more rows to update, try adain tomorrow, Waiting {Settings.AppSettings.RunningInteval} ms for next batch");
				Thread.Sleep(Settings.AppSettings.RunningInteval);
			}

		}
	}
}
