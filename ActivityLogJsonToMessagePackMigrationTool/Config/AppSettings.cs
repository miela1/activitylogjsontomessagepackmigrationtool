﻿using Newtonsoft.Json.Linq;

namespace ActivityLogJsonToMessagePackMigrationTool.Config
{
	public class AppSettings
	{
		public AppSettings(JToken json)
		{
			RunningInteval = int.Parse(json["RunningInteval"].ToString());
			BatchSize = int.Parse(json["BatchSize"].ToString());
		}

		public int RunningInteval { get; }
		public int BatchSize { get; }
	}
}
