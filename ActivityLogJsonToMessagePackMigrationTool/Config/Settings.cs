﻿using Newtonsoft.Json.Linq;
using System.IO;
using System.Reflection;
using System.Text;

namespace ActivityLogJsonToMessagePackMigrationTool.Config
{

	public static class Settings
	{
		static string location = Assembly.GetEntryAssembly().Location;
		public static string StartupPath => Path.GetDirectoryName(location);
		static JObject raw = JsonHelpers.ReadJson(Path.Combine(StartupPath,"Config" ,"Settings.json"));

		public static AppSettings AppSettings = new AppSettings(raw["AppSettings"]);
		public static Databases Databases = new Databases(raw["Databases"]);
	}




	public static class JsonHelpers
	{
		public static JObject ReadJson(string filepath)
		{
			//Log.Console.Information("Reading settings file", filepath);
			using var sr = new StreamReader(filepath, Encoding.ASCII);
			var jsonString = sr.ReadToEnd();
			return JObject.Parse(jsonString);
		}
	}
}
