﻿using System.Collections.Generic;
using System.Data;
using ActivityLogJsonToMessagePackMigrationTool.Core.Dataitems;

namespace ActivityLogJsonToMessagePackMigrationTool.Core
{
	public class Datafactory
	{
		public List<ActivityLogDbData> GetActivityLogItems(int Count) {
			const string sql = @"SELECT TOP (@Count) CustomerId, UserId, TimeStamp, Url, Action, JsonData
								 FROM ActivityLog
								 WHERE MessagePackData IS NULL
								 ORDER BY Timestamp DESC";

			var result = new List<ActivityLogDbData>();
			using (var da = DataAccess.WebtrackStatsCollection) {
				var param = da.MakeParameter("@Count", Count, SqlDbType.Int);
				var reader = da.ExecuteQuery(sql, param);

				while (reader.Read()) {
					var tmp = new ActivityLogDbData {
						CustomerId = reader.GetInt32("CustomerId"),
						UserId = reader.GetInt32("UserId"),
						Timestamp = reader.GetDateTime("Timestamp"),
						Action = reader.GetString("Action"),
						JsonData = reader.GetString("JsonData"),
						Url = reader.GetString("Url")
					};

					result.Add(tmp);
				}
			}

			return result;
		}

		public int SaveActivityLogItems(List<ActivityLogDbData> data) {
			const string sql = @"UPDATE ActivityLog
								 SET MessagePackData = @Data, JsonData = ''
								 WHERE CustomerId = @CustomerId
								   AND UserId = @UserId
								   AND[TimeStamp] = @TimeStamp
								   AND[Url] = @Url
								   AND[Action] = @Action";

			using (var da = DataAccess.WebtrackStatsCollection) {
				//Log.Console.Information($"Writing {data.Count} items to database");
				da.OpenConnection();
				da.UseTransactions("ActivityLogUpdate");

				var count = 0;
				foreach (var item in data) {
					if (item.MessagePackData == null)
						continue;

					var packData = da.MakeParameter("@Data", item.MessagePackData, SqlDbType.VarBinary, 2048);
					var customerId = da.MakeParameter("@CustomerId", item.CustomerId, SqlDbType.Int);
					var userId = da.MakeParameter("@UserId", item.UserId, SqlDbType.Int);
					var timeStamp = da.MakeParameter("@TimeStamp", item.Timestamp, SqlDbType.DateTime2, 3);
					var url = da.MakeParameter("@Url", item.Url, SqlDbType.VarChar, 1000);
					var action = da.MakeParameter("@Action", item.Action, SqlDbType.VarChar, 15);

					count += da.Execute(sql, packData, customerId, userId, timeStamp, url, action);
				}
				//Log.Console.Information($"{count} rows ready for commit");
				da.CommitTransaction();

				return count;
			}
		}
	}
}
